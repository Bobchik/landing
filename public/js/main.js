$(document).ready(function () {
    $('.yellow').hover(function () {
            $('.bg-text').css({"color": "yellow", "opacity": "100"});
        },
        function () {
            $('.bg-text').css({"color": "#000", "opacity": "0.03"});
        });

    $('.green').hover(function () {
            $('.bg-text').css({"color": "green", "opacity": "100"});
        },
        function () {
            $('.bg-text').css({"color": "#000", "opacity": "0.03"});
        });

    //scroll menu items
    var menuItem = $('.nav-scroller .nav li a');
    var sections = $('body section').toArray();

    // checking section on scroll
    function checkSection() {
        for(var i = 0; i < sections.length; i++) {
            if(($(sections[i]).offset().top - $(window).scrollTop()) < 20) {
                var id = $(sections[i]).attr('id');
                activeSection = id;
            }
        }

        if(activeSection == '') {
            $(menuItem[0]).addClass('active');
        } else if($(window).scrollTop() + $(window).height() == $(document).height()) {
            $(menuItem).removeClass('active');
            $(menuItem[sections.length - 1]).addClass('active');
        } else {
            $(menuItem).removeClass('active');
            $('.nav-scroller .nav li').find('a[href="#' + activeSection + '"]').addClass('active');
        }
    }

    // menuItem.click(function() {
    //     var id = $(this).find('a').attr('href');
    //
    //     $("html, body").animate({ scrollTop: $(id).offset().top }, 1500);
    // });

    $('a').click(function(){
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 1000);
        return false;
    });

    $(window).scroll(function() {
        checkSection();
    });

    setTimeout(function(){
        $('#modal-popup').modal();
    }, 5000);

    $('.download').click(function () {
        var name = $("#name").val();
        var email = $("#email").val();
       $('#modal-popup').modal('hide');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "post",
            url: "/create",
            data: {
                name: name,
                email: email
            }
        });
    });
});