<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('banner');
});

Route::get('/landing', function () {
    return view('landing');
});

Route::get('/signup', 'UserController@signup');
Route::get('/login', 'UserController@login');

Route::post('/create', 'UserController@create');
Route::get('/admin', 'AdminController@admin');
Route::post('/admin', 'AdminController@send');
