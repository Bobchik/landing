<?php

namespace App\Http\Controllers;

use App\Mail\SendText;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    public function admin()
    {
        $dates = array();

        $dates = DB::table('users')->select('created_at')->get()->unique();

        return view('admin', compact('dates'));

    }

    public function send()
    {
        $date_from = request()->date_from;
        $date_to = request()->date_to;
        $message = request()->admin_text;

        $users = DB::table('users')
            ->where('created_at', '>=', $date_from)
            ->where('created_at', '<=', $date_to)
            ->get()
            ->toArray();

        foreach ($users as $user) {
            $message = strtr($message, array('{NAME}' => $user->name));
            Mail::to($user->email)->send(new SendText($message));
        }

    }
}
