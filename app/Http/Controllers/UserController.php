<?php

namespace App\Http\Controllers;

use App\Mail\FileMail;
use App\User;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function signup()
    {
        return view('signup');
    }

    public function login()
    {
        return view('login');
    }

    public function create()
    {
        $user = new User();
        $user->name = request()->name;
        $user->email = request()->email;

        $user->save();

        Mail::to(request()->email)->send(new FileMail());
    }
}
