@extends('welcome')

@section('content')
    <div class="container">
        <h2 class="text-center">Admin panel</h2>
        <div class="row">
            <div class="col-md-12">
                <form action="{{ url('/admin') }}" method="post" id="admin-form">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="exampleFormControlSelect1">Date from</label>
                            <select class="custom-select d-block w-100" id="exampleFormControlSelect1" name="date_from">
                                @foreach($dates as $date)
                                    <option>{{ $date->created_at }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="exampleFormControlSelect1">Date to</label>
                            <select class="custom-select d-block w-100" id="exampleFormControlSelect2" name="date_to">
                                @foreach($dates as $date)
                                    <option>{{ $date->created_at }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="admin-text">Message</label>
                        <textarea class="form-control" name="admin_text" id="admin-text" cols="30"
                                  rows="10"></textarea>
                    </div>
                    <button class="btn btn-primary" type="submit">Send</button>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection