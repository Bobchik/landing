@extends('welcome')

@section('content')
    <div class="text-center">
        <form class="form-signin">
            <img class="mb-4" src="{{asset('storage/img/logo.png')}}" alt="" style="margin-top: 60px;">
            <div class="sign-up-text">
                <p class="text-center">Sign up</p>
            </div>
            <div class="col-md-4 offset-4 form">
                <p class="text-center">Account details</p>

                <input type="text" id="companyName" class="form-control" placeholder="Company/Name *" required
                       autofocus>
                <input type="text" id="address1" class="form-control" placeholder="Address 1 *" required>
                <input type="text" id="address2" class="form-control" placeholder="Address 2" required autofocus>
                <input type="text" id="city" class="form-control" placeholder="City *" required>
                <input type="text" id="contry" class="form-control" placeholder="Country *" required autofocus>
                <input type="text" id="region" class="form-control" placeholder="Region/State *" required>
                <input type="text" id="zip" class="form-control" placeholder="ZIP *" required autofocus>
                <input type="text" id="phone" class="form-control" placeholder="Phone" required>

                <br>
                <br>

                <p class="text-center">User details</p>
                <input type="text" id="email" class="form-control" placeholder="Email *" required>
                <input type="text" id="fname" class="form-control" placeholder="First name *" required>
                <input type="text" id="lname" class="form-control" placeholder="Last name *" required>
                <input type="text" id="password" class="form-control" placeholder="Password *" required>
                <input type="text" id="confirm" class="form-control" placeholder="Confirm password *" required>

                <br>
                <br>

                <p class="text-center">Additional questions</p>
                <input type="text" id="skype" class="form-control" placeholder="Skype is required. What`s your name? *"
                       required>
                <input type="text" id="traf" class="form-control" placeholder="Please state your sources of traffic *" required>
                <input type="text" id="lname" class="form-control" placeholder="Is your traffic incentivised/non-incentivised? Both? *" required>
                <input type="text" id="site" class="form-control" placeholder="What`s your company website URL? *" required>
                <textarea type="text" class="form-control" id="message" placeholder="Please provide us with more information about app you would like to promote (preview URLs, payouts, GEOs) *"></textarea>
                <br>

                <div class="checkbox mb-3">
                    <label>
                        <input type="checkbox" value="remember-me" class="check"> I agree with <a href="#" class="term">Term
                            & Conditions</a>
                    </label>
                </div>
                <div>
                    <a href="#" class="btn sign-up-btn text-center"><span class="sign-up-link">Sign up</span></a>
                </div>

                <br>
                <br>
                <p class="text-center">Already have an account?</p>
                <a href="{{ url('/login') }}" class="btn login-up-btn text-center"><span class="sign-up-link">Login</span></a>
            </div>
            <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
        </form>
    </div>
@endsection