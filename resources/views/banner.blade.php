@extends('welcome')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-3 offset-2">
                <a href="{{url('landing')}}"><img src="{{asset('storage/img/banner.jpg')}}" alt=""></a>
            </div>
        </div>
    </div>
@endsection