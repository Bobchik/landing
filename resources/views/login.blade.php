@extends('welcome')

@section('content')
    <div class="text-center">
        <form class="form-signin">
            <img class="mb-4" src="{{asset('storage/img/logo.png')}}" alt="" style="margin-top: 60px;">
            <div class="sign-up-text">
                <p class="text-center">Welcome</p>
            </div>
            <br>
            <div class="col-md-4 offset-4 form">
                <p class="text-center"></p>

                <input type="text" id="Login" class="form-control" placeholder="Login *" required
                       autofocus>
                <input type="password" id="Password" class="form-control" placeholder="Password *" required>

                <div class="checkbox mb-3">
                    <label>
                        <input type="checkbox" value="remember-me" class="check"> I agree with <a href="#" class="term">Term
                            & Conditions</a>
                    </label>
                </div>
                <div>
                    <a href="#" class="btn sign-up-btn text-center"><span class="sign-up-link">Login up</span></a>
                </div>

                <br>
                <br>
                <p class="text-center">Already have an account?</p>
                <a href="{{ url('/signup') }}" class="btn login-up-btn text-center"><span class="sign-up-link">Login</span></a>

            </div>
            <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
        </form>
    </div>
@endsection