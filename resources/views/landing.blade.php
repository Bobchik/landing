@extends('welcome')

@section('content')
    <section id="header">
        <div class="flex-container">
            @include('navbar')
            <div class="row">
                <div class="col-md-6 col-xs-6 col-sm-6 d-flex flex-column align-items-center offset-6">
                    <div class="logo">
                        <img src="{{asset('storage/img/logo.png')}}" alt="logo">
                    </div>
                    <div class="header-text">
                        <p class="text-center">Your victorious ads</p>
                    </div>
                    <div class="sign-up">
                        <a href="{{ url('/signup') }}" class="btn sign-up-btn text-center"><span class="sign-up-link">Sign up</span></a>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section id="about">
        <div class="flex-container d-flex align-items-center">
            <div class="bg-text d-block">
                <div>About us</div>
            </div>
            <div class="col d-flex flex-column justify-content-around">
                <div class="text justify-content-around align-self-center green">We are here for you to make your
                    advertising
                    work. For several
                    years we carefully studied
                    the mobile advertising market, learned all its secrets, strengths and weaknesses. We always pay
                    great
                    attention to the subtleties and nuances, and due to this, we can direct the mobile advertising as
                    quickly and efficiently as possible.
                </div>
                <div class="text align-items-end align-self-center green">The team of our highly motivated specialists
                    of
                    the highest level is ready to immediately help
                    you, answer all the questions that arise and help both advertisers and publishers achieve maximum
                    results in a short time.
                </div>
            </div>
            <div class="col d-flex flex-column justify-content-center">
                <div class="text align-self-center yellow">We provide our customers with a high-quality service based on
                    the latest
                    technologies and
                    in-depth data analytics. The customization and flexibility of our platform will allow you to
                    discover
                    the new highest standards for CPI / CPА / CPM advertising campaigns.
                </div>
            </div>
        </div>

    </section>
    <section id="publisher">
        <div class="flex-container d-flex align-items-center">
            <div class="d-flex flex-wrap bg-text bg-publisher">
                <div class="block">
                    <div class="flex-item justify-content-start">For</div>
                    <div class="flex-item justify-content-start bg-sec">publishers</div>
                </div>
            </div>
            <div class="col d-flex flex-column justify-content-around">
                <div></div>
                <div class="text text-b align-self-center green">
                    Working with us you choose only among the best offers. We will provide you with the most accurate
                    and
                    up-to-date tracking system in real time and API integration in a few easy steps. Our team of experts
                    is
                    always here to help.
                </div>
            </div>
            <div class="col d-flex flex-column justify-content-around">
                <div class="text text-b align-self-center yellow">
                    The main goal of each publisher is to adjust the flow of their mobile traffic so that they bring the
                    maximum profit. Do you still think that this is easier said than done? You tried a lot of options,
                    but
                    none of the platforms brought you the planned result? Do you think that other publishers are
                    successful
                    because of some magic or a secret recipe? We know this secret and can make it work for you.
                </div>
                <div></div>
            </div>
        </div>
    </section>
    <section id="advertiser">
        <div class="flex-container d-flex align-items-center">
            <div class="d-flex flex-wrap bg-text bg-publisher">
                <div class="block">
                    <div class="flex-item justify-content-start">For</div>
                    <div class="flex-item justify-content-start bg-sec">advertiser</div>
                </div>
            </div>
            <div class="col d-flex flex-column justify-content-around">
                <div></div>
                <div class="text align-self-center green">
                    The most accurate targeting. Fine-tuned Ad campaigns. The best anti-fraud technologies. Direct
                    access to
                    the most successful publishers and high-priority traffic. All these factors will work for you, bring
                    you
                    market leadership and impressively increase your ROI.
                </div>
            </div>
            <div class="col d-flex flex-column justify-content-around">
                <div class="text align-self-center yellow">
                    To deliver your Ads to every potential customer. To hook on the narrowest niches and the most
                    hard-to-reach audiences. Advertisers face these challenges every day and often they determine the
                    success of the campaign. You think that you have tried everything, but the result still does not
                    please
                    you? We are here to come to your aid.
                </div>
                <div></div>
            </div>
        </div>
    </section>
    <section id="contacts">
        <div class="flex-container d-flex align-items-center">
            <div class="bg-text contacts">
                <div>Contacts</div>
            </div>
            <div class="col d-flex flex-column justify-content-center">
                <div class="text text-b align-self-center green">
                    Do not delay, start making profits today! Just fill out the form below and our team will contact you
                    as
                    soon as possible.
                </div>
            </div>
            <div class="col d-flex flex-column justify-content-around">
                <div class="text text-b yellow">
                    Are you a publisher and want to turn your traffic into gold? Are you an advertiser and want your
                    advertising campaign to clearly reach the customers? In both cases, you are in the right place.
                </div>
                <div class="contact-form">
                    <form action="#">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Your name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <textarea type="text" class="form-control" id="message" placeholder="Message"></textarea>
                        </div>
                        <div class="contact">
                            <button type="submit" class="btn sign-up-btn text-center"><span
                                        class="sign-up-link">Submit</span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal-popup" tabindex="-1" role="dialog" aria-labelledby="modal-popup-label"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center" id="modal-popup-label">Input pls your e-mail and name</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="#" role="form">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" placeholder="E-mail">
                        </div>
                        <button class="btn btn-primary download">Download</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

