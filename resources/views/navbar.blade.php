<div class="navbar navbar-collapse d-flex justify-content-center flex-items-xs-middle fixed-top" id="navbar">
    <div class="nav-scroller py-1 mb-2">
        <ul class="nav navbar-nav d-flex justify-content-between align-items-center list-unstyled">
            <li><a href="#about">About</a></li>
            <li><a href="#publisher">For Publisher</a></li>
            <li><a href="#advertiser">For Advertiser</a></li>
            <li><a href="#contacts">Contacts</a></li>
            <li><a href="{{ url('/signup') }}">Sign up</a></li>
            <li><a href="{{ url('/login') }}">Login</a></li>
        </ul>
    </div>
</div>